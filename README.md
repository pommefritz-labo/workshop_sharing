# Workshop App

## 開発環境構築
1. Azureにアプリを登録
- このページの手順に従ってアプリをAzureに登録
https://docs.microsoft.com/ja-jp/graph/tutorials/react?tutorial-step=2

2. Config.jsの作成
- Config.js.exampleをコピー
```
cp src/auth/Config.js.example src/auth/Config.js
```
- Config.jsの"appId"にAzureのアプリケーションIDを入力

3. MongoDBのインストール
* (4/27追記)`docker-compose up --build`コマンドでMongoDBを起動させる
4. アプリ起動
- yarnをインストール
```
npm install -g yarn
```
5. frontサーバ起動 
```
cd front
yarn install
yarn start
```
6. APIサーバ起動 
```
cd api
yarn install
yarn start
```

### Dockerを使用する場合
1. api/db/index.jsの
'mongodb://localhost:27017/note'  
を
'mongodb://mongodb:27017/note'  
に変更する
2. 下記コマンドでDockerを起動させる 
```
docker-compose up --build
```
## フォルダ構成案
- フォルダ構成のためにAtomic Designを採用
### Container ComponentとPresentational Component
#### Container Component
- Classベースのコンポーネント
- 下位コンポーネントにデータを受け渡すためのコンポーネント

#### Presentational Component
- Functionベースのコンポーネント
- 描画担当のコンポーネント

### src
- src直下にindex.jsやApp.jsなどpagesに当たるものを配置
#### templates
- Container Componentの中で他のContainer Componentにデータを渡すもの
#### organisms
- Headerなどページの部分を構成するもの
- Container Component
#### molecules
- Presentational Component 
#### atoms
- Presentational Componentの中で他と依存性のないもの
#### auth
- Config.jsを配置する場所
#### api
- MS Graphとbackendとの通信用のAPI
#### redux
- reduxに関連するもの。作成中
