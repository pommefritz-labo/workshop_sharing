import React from 'react';
import config from '../auth/Config';
import api from '../api/backend/index';
import NoteEdit from '../molecules/NoteEdit.jsx';
import NoteList from '../atoms/NoteList.jsx';
import NoteDetail from '../molecules/NoteDetail.jsx';
import NoteCreate from '../molecules/NoteCreate.jsx';
import { getUsers, getGroups, getUserDetails, getOneNote } from '../api/msgraph/GraphService';
import List from '@material-ui/core/List';
import { BrowserRouter as Router, Route } from 'react-router-dom';

export default class KnowledgeComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      notes: [],
    };
  }

  async componentDidMount() {
    try {
      await api.getAllNotes()
        .then((notes) => {
          this.setState({
            notes: notes.data.data
          })
        })
    }
    catch(err) {
      console.error(err)
    }
  }

  componentDidUpdate() {
  }

  render() {
    // console.log("notes props: ", this.props)
    return (
      <>
      <Route exact path={`${this.props.match.path}`}
        render={(props) =>
          <NoteList {...props}
            notes={this.state.notes}
          />
         }
      />
      <Route exact path={`${this.props.match.path}/create`}
        render={(props) =>
          <NoteCreate {...props} />
         }
      />
      <Route exact path={`${this.props.match.path}/:noteId`}
        render={(props) =>
          <NoteDetail {...props}
            notes={this.state.notes}
          />
         }
      />
      <Route exact path={`${this.props.match.path}/edit/:noteId`}
        render={(props) =>
          <NoteEdit {...props}
            notes={this.state.notes}
          />
         }
      />
      </>
    );
  }
}

