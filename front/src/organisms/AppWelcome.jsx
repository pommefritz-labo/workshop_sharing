import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const buttonContainerStyle = {
  display: "flex",
  justifyContent: "center",
}

const appHeaderProfile = {
  textAlign: "center",
}

function ButtonLogin(props) {
  if (props.isAuthenticated) {
    return (
      <h4 style={appHeaderProfile}>ようこそ {props.user.displayName}さん</h4>
    );
  }

  return (
    <>
      <h4 style={appHeaderProfile}>Workshopへようこそ</h4>
      <div style={buttonContainerStyle}>
        <Button
          variant="contained"
          color="primary"
          onClick={props.loginMethod}
        >log in</Button>
      </div>
    </>
  );
}

export default class Welcome extends React.Component {
  render() {
    return (
      <ButtonLogin
        isAuthenticated={this.props.isAuthenticated}
        user={this.props.user}
        loginMethod={this.props.loginMethod} />
    );
  }
}
