import React from 'react';
import config from '../auth/Config';
import { getEvents } from '../api/msgraph/GraphService';
import BigCalendar from '../molecules/BigCalendar.jsx'

export default class CalendarComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      events: [],
    };
  }

  async componentDidMount() {
    try {
      const accessToken = await window.msal.acquireTokenSilent({
        scopes: config.scopes
      });
      const events = await getEvents(accessToken);
      // console.log("raw e:", events)
      this.setState({events: events.value});
    }
    catch(err) {
      this.props.showError('ERROR', JSON.stringify(err));
    }
  }

  render() {
    return (
      <BigCalendar events={this.state.events} />
    );
  }
}
