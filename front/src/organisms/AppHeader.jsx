import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
  Button,
  AppBar,
  Toolbar,
  Typography
} from '@material-ui/core';
import useReactRouter from 'use-react-router';

const useStyles = makeStyles((theme) => ({
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  appbar: {
    // zindex: theme.zIndex.drawer + 1,
  }
}));

function HeaderRender(props) {
  const classes = useStyles();
  const theme = useTheme();

  if (props.isAuthenticated) {
    return (
      <Toolbar className={classes.toolbar}>
        <div></div>
        <Typography variant="h3">Workshop</Typography>
        <Button color="inherit" onClick={props.logoutButton}>logout</Button>
      </Toolbar>
    );
  }

  return (
    <Toolbar className={classes.toolbar}>
      <div></div>
      <Typography variant="h3">Workshop</Typography>
      <Typography variant="h6">Not login</Typography>
    </Toolbar>
  );
}

export default function renderHeader(props) {
  const {logoutMethod, isAuthenticated, ...others} = props
  return (
    <AppBar position="sticky" className="classes.appbar">
      <HeaderRender
        logoutButton={logoutMethod}
        isAuthenticated={isAuthenticated} />
    </AppBar>
  );
}
