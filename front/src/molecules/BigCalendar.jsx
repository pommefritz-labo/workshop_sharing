import React from 'react';
import moment from 'moment';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { makeStyles } from '@material-ui/core/styles';
import CalendarDialog from './CalendarDialog.jsx';

const localizer = momentLocalizer(moment);

/*
 *  BigCalendar用のEventsオブジェクトと
 *  MS GraphへHTTP PATCHメソッドを送信するためのEventsオブジェクトの二つを作成する
 *  BigCalendar用のEventsオブジェクトにIDを付けて、
 *  IDと紐づいたMS Graph PATCH用のEventもCalendarDialogに渡す
 */

const addPropsForBigCalendar = (events) => {
  let retArr = []
  events.map((obj, index) => {
    retArr[index] = {
      id: index,
      title: obj.subject,
      attendees: obj.attendees,
      start: obj.start.dateTime,
      end: obj.end.dateTime,
    }
  })

  return retArr
}

const BigCalendar = (props) => {
  const {events, ...elems} = props

  const [open, setOpen] = React.useState(false);
  const [bigCalendarEvent, setBigCalendarEvent] = React.useState({});
  const [eventForPatch, setEventForPatch] = React.useState({});

  const handleClose = () => {
    setOpen(false);
    setBigCalendarEvent({});
  };

  const onSelectEvent = ((event, e) => {
    setOpen(true);
    setBigCalendarEvent(event);
    setEventForPatch(events[event.id]);
    }
  )

  return (
    <div>
      <Calendar
        localizer={localizer}
        events={addPropsForBigCalendar(events)}
        startAccessor="start"
        endAccessor="end"
        defaultView="month"
        style={{ height: 500 }}
        onSelectEvent={onSelectEvent}
        popup={true}
        views={["month"]}
      />
      <CalendarDialog
        bigCalendarEvent={bigCalendarEvent}
        open={open}
        onClose={handleClose}
        eventForPatch={eventForPatch}
      />
    </div>
  )
}

export default BigCalendar;
