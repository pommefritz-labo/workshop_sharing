import ReactDOM from 'react-dom'
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';

import NoteEditor from '../atoms/NoteEditor.jsx'

const initialSource = `
# Live demo  
Changes are automatically rendered as you type.  
## Table of Contents  
* Implements [GitHub Flavored Markdown](https://github.github.com/gfm/)  
`

export default function renderNoteCreate(props) {
  const [ title, setTitle ] = useState("")
  const [ body, setBody ] = useState("")
  const { noteId } = useParams();

  const handleBodyChange = (e) => {
    setBody(e.target.value)
  }

  const handleTitleChange = (e) => {
    e.preventDefault()
    setTitle(e.target.value)
  }

  return (
    <>
    <TextField
      label="Title"
      variant="standard"
      value={title}
      onChange={(e) => handleTitleChange(e)}
     />
    <NoteEditor
      value={body}
      title={title}
      onChange={(e) => handleBodyChange(e)}
    />
    </>
  );
}

