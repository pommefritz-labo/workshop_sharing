import React, { useState, useEffect } from 'react';
import useReactRouter from 'use-react-router';
import { useParams } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ReactMarkdown from 'react-markdown';
import api from '../api/backend/index';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

export default function renderNoteDetail(props) {
  const [ note, setNote ] = useState({})
  const { notes, ...others } = props
  const { history, location, match } = useReactRouter();
  const { noteId } = useParams();
  const handleClick = (e, link) => {
    e.preventDefault();
    history.push(link)
    return
  }
  useEffect(() => {
    const fetchData = async() => {
      await api.getNoteById(noteId)
        .then((n) => {
          setNote(n.data.data)
        })
    }
    fetchData()
  }, [])
  return (
    <div>
      <Typography
        variant="h2"
      >{note.title}</Typography>
      <ReactMarkdown
        source={note.body}
      >
      </ReactMarkdown>
      <p>{note.author}</p>
      <Button
        color="primary"
        onClick={(e) => handleClick(e, `edit/${note._id}`)}
      >編集</Button>
    </div>
  );
}
