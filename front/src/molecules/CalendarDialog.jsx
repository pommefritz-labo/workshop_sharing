import React from 'react';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { updateEvents, getUserDetails } from '../api/msgraph/GraphService'
import config from '../auth/Config';

moment.locale('ja');

const useStyles = makeStyles({
  listContainer: {
    display: "flex",
  },
});

const CalendarDialog = (props) => {
  const classes = useStyles();
  const { onClose, open, bigCalendarEvent, eventForPatch, ...others } = props;

  const handleClose = () => {
    onClose(bigCalendarEvent);
  };

  const handleListItemClick = (value) => {
    onClose(value);
  };

  const renderDate = (date) => {
    if (!date) {
      return
    }
    return moment(date).format("YYYY/MM/DD hh:mm:ss")
  }

  const renderNumberOfAttendees = (attendees) => {
    if (!attendees) {
      return
    }
    let attend = 0
    let cancel = 0
    let unknown = 0
    attendees.map((a, i) => {
      switch (a.status.response) {
        case 'none':
          attend += 1
          break
        case 'organizar':
          attend += 1
          break
        case 'accepted':
          attend += 1
          break
        case 'tentativelyaccepted':
          attend += 1
          break
        case 'notresponded':
          unknown += 1
          break
        case 'declined':
          cancel += 1
          break
        default:
          unknown += 1
      }
    })
    return (
      <List className={classes.listContainer}>
        <ListItemText>参加者{attend}人</ListItemText>
        <ListItemText>キャンセル・未定{cancel + unknown}人</ListItemText>
      </List>
    );
  }

/*
 *HTTP PATCHでAttendeesのデータを何回も送ると、
 *Attendeesの配列にデータがスタックされる。
 *リロードすると、displayNameかmailあたりが同じデータがまとめられて、
 *一つになっている。
 *データのまとめ方はDate順だと思われる。
 */
  async function attendClick(event) {
    try {
      // TODO getUserDetailはReduxを使用してそこにデータをためておく
      const accessToken = await window.msal.acquireTokenSilent({
        scopes: config.scopes
      });
      const userDetailRes = await getUserDetails(accessToken)
      event.attendees.push({
        type: "required",
        status: {response: "accepted", time: (new Date).toISOString()},
        emailAddress: {
          name: userDetailRes.displayName,
          address: userDetailRes.mail || userDetailRes.userPrincipalName,
        }
      })
      const res = await updateEvents(accessToken, event);
      console.log("res", res)
    }
    catch(err) {
      console.error(JSON.stringify(err))
    }
  }

  async function cancelClick(event) {
    try {
      const accessToken = await window.msal.acquireTokenSilent({
        scopes: config.scopes
      });
      if (accessToken) {
        const userDetailRes = await getUserDetails(accessToken)
        event.attendees.map((a, i) => {
          // Is mail better than displayname?
          if (a.emailAddress.name === userDetailRes.displayName) {
            event.attendees[i] = {
              type: "required",
              status: {response: "declined", time: (new Date).toISOString()},
              emailAddress: {
                name: userDetailRes.displayName,
                address: userDetailRes.mail || userDetailRes.userPrincipalName,
              }
            }
          }
        })
        const res = await updateEvents(accessToken, event);
        console.log("res", res)
      }
    }
    catch(err) {
      console.log(JSON.stringify(err))
    }
  }

    return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">{bigCalendarEvent.title}</DialogTitle>
        <List>
          <ListItemText>開始時間: {renderDate(bigCalendarEvent.start)}</ListItemText>
          <ListItemText>終了時間: {renderDate(bigCalendarEvent.end)}</ListItemText>
        </List>
        {renderNumberOfAttendees(bigCalendarEvent.attendees)}
      <Button variant="contained" color="primary" onClick={() => attendClick(eventForPatch)}>Attend</Button>
      <Button variant="contained" color="secondary" onClick={() => cancelClick(eventForPatch)}>Cancel</Button>
    </Dialog>
  );
}

export default CalendarDialog;
