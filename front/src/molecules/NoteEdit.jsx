import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom'
import { useParams } from 'react-router-dom';
import api from '../api/backend/index';
import TextField from '@material-ui/core/TextField';

import NoteEditor from '../atoms/NoteEditor.jsx'

export default function renderNoteEdit(props) {
  const [ title, setTitle ] = useState("")
  const [ body, setBody ] = useState("")
  const [ note, setNote ] = useState({})
  const { noteId } = useParams()

  const handleBodyChange = (e) => {
    setBody(e.target.value)
  }

  const handleTitleChange = (e) => {
    e.preventDefault()
    setTitle(e.target.value)
  }

  useEffect(() => {
    const fetchData = async() => {
      await api.getNoteById(noteId)
        .then((n) => {
          setNote(n.data.data)
          setBody(n.data.data.body)
          setTitle(n.data.data.title)
        })
    }
    fetchData()
  }, [])

  return (
    <>
      <TextField
        label="Title"
        variant="standard"
        value={title}
        onChange={(e) => handleTitleChange(e)}
       />
      <NoteEditor
        value={body}
        title={title}
        onChange={(e) => handleBodyChange(e)}
      />
    </>
  );
}

