import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import EventIcon from '@material-ui/icons/Event';
import NoteIcon from '@material-ui/icons/Note';
import useReactRouter from 'use-react-router';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import CalendarComponent from '../organisms/CalenderComponent.jsx';
import AppWelcome from '../organisms/AppWelcome.jsx';
import AppHeader from '../organisms/AppHeader.jsx';
import NoteComponent from '../organisms/NoteComponent.jsx';
import NoteDetail from '../molecules/NoteDetail.jsx';
import NoteCreate from '../molecules/NoteCreate.jsx';
import ErrorMessage from '../atoms/ErrorMessage.js';

const drawerOpenWidth = 240;
const drawerCloseWidth = 73;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    width: drawerOpenWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerOpenWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    //width: theme.spacing(7) + 1,
    //[theme.breakpoints.up('sm')]: {
    //  width: theme.spacing(9) + 1,
    //},
    width: drawerCloseWidth,
  },
  mainOpen: {
    width: 'calc(100vw - 240px)',
  },
  mainClose: {
    width: 'calc(100vw - 73px)',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
}));

export default function AppDrawer(props) {
  const {loginError, loginMethod, logoutMethod, user, isAuthenticated, ...elms} = props
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const { history, location, match } = useReactRouter();

  const handleDrawer = () => {
    if (open === true) {
      setOpen(false)
    } else {
      setOpen(true)
    }
  }

  const renderLoginError = (e) => {
    if (e !== null) {
      return (
        <ErrorMessage message={loginError.message} debug={loginError.debug} />
      );
    }
    return (
      <></>
    );
  }

  const renderDrawerIcon = (text) => {
    if (text === "Calendar") {
      return (
        <EventIcon />
      );
    } else if (text === "Note") {
      return (
        <NoteIcon />
      );
    } else if (text === "Dashboard") {
      return (
        <InboxIcon />
      );
    }
  }


  const handleClick = (e, text) => {
    e.preventDefault();
    if (text === 'Calendar') {
      history.push('')
      return
    }
    history.push(text.toLowerCase())
    return
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawer}>
            {open === true ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
          {['Dashboard', 'Calendar', 'Note'].map((text, index) => (
            <ListItem button key={text} onClick={(e) => handleClick(e, text)}>
              <ListItemIcon>{renderDrawerIcon(text)}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        <Divider />
      </Drawer>
      <main
        className={clsx({
          [classes.mainOpen]: open,
          [classes.mainClose]: !open,
        })}
      >
        <AppHeader
          logoutMethod={logoutMethod}
          loginMethod={loginMethod}
          isAuthenticated={isAuthenticated}
        />
        <AppWelcome
          isAuthenticated={isAuthenticated}
          loginMethod={loginMethod}
          user={user}
        />
        {renderLoginError(loginError)}
        <Route exact path="/"
          render={(props) =>
            <CalendarComponent
              {...props}
            />
           }
        />
        <Route path="/note"
          render={(props) =>
            <NoteComponent
              {...props}
            />
           }
        />
      </main>
    </div>
  );
}
