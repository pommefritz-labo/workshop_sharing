import React, { Component } from 'react';
import ErrorMessage from '../atoms/ErrorMessage';
import AppDrawerContainer from '../templates/AppDrawerContainer.jsx';
import CalendarComponent from '../organisms/CalenderComponent.jsx';
import NoteComponent from '../organisms/NoteComponent.jsx';
import NoteDetail from '../molecules/NoteDetail.jsx';
import NoteCreate from '../molecules/NoteCreate.jsx';
import config from '../auth/Config';
import { getUserDetails } from '../api/msgraph/GraphService';
import { UserAgentApplication } from 'msal';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

class AuthContainer extends Component {
  constructor(props) {
    super(props);

    // console.log(JSON.stringify(props));

    this.userAgentApplication = new UserAgentApplication({
        auth: {
            clientId: config.appId,
            redirectUri: config.redirectUri
        },
        cache: {
            cacheLocation: "localStorage",
            storeAuthStateInCookie: true
        }
    });

    var user = this.userAgentApplication.getAccount();

    this.state = {
      isAuthenticated: (user !== null),
      user: {},
      error: null
    };

    if (user) {
      // Enhance user object with data from Graph
      this.getUserProfile();
    }
  }

  render() {
    return (
      <Router>
        <Switch>
          <AppDrawerContainer
            loginError={this.state.error}
            logoutMethod={this.logout.bind(this)}
            loginMethod={this.login.bind(this)}
            isAuthenticated={this.state.isAuthenticated}
            user={this.state.user} />
        </Switch>
      </Router>
    );
  }

  async login() {
    try {
      await this.userAgentApplication.loginPopup(
        {
          scopes: config.scopes,
          prompt: "select_account"
      });
      await this.getUserProfile();
    }
    catch(err) {
      var error = {};

      if (typeof(err) === 'string') {
        var errParts = err.split('|');
        error = errParts.length > 1 ?
          { message: errParts[1], debug: errParts[0] } :
          { message: err };
      } else {
        error = {
          message: err.message,
          debug: JSON.stringify(err)
        };
      }

      this.setState({
        isAuthenticated: false,
        user: {},
        error: error
      });
    }
  }

  logout() {
    this.userAgentApplication.logout();
  }

  async getUserProfile() {
    try {
      // Get the access token silently
      // If the cache contains a non-expired token, this function
      // will just return the cached token. Otherwise, it will
      // make a request to the Azure OAuth endpoint to get a token

      var accessToken = await this.userAgentApplication.acquireTokenSilent({
        scopes: config.scopes
      });

      if (accessToken) {
        // Get the user's profile from Graph
        var user = await getUserDetails(accessToken);
        this.setState({
          isAuthenticated: true,
          user: {
            displayName: user.displayName,
            email: user.mail || user.userPrincipalName
          },
          error: null
        });
      }
    }
    catch(err) {
      var error = {};
      if (typeof(err) === 'string') {
        var errParts = err.split('|');
        error = errParts.length > 1 ?
          { message: errParts[1], debug: errParts[0] } :
          { message: err };
      } else {
        error = {
          message: err.message,
          debug: JSON.stringify(err)
        };
      }

      this.setState({
        isAuthenticated: false,
        user: {},
        error: error
      });
    }
  }
}

export default AuthContainer;
