import React, { Component } from 'react';
import { UserAgentApplication } from 'msal';
import AuthContainer from './templates/AuthContainer.js'
import './App.css';

class App extends Component {
  render() {
    return (
      <AuthContainer />
    )
  }
}

export default App;
