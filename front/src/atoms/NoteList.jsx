import React from 'react';
import api from '../api/backend/index';
import useReactRouter from 'use-react-router';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import { makeStyles, useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  noteTitle: {
    alignItems: 'center',
  },
}));

export default function renderNoteList(props) {
  const classes = useStyles();
  const theme = useTheme();

  const { notes, ...elems } = props
  const { history, location, match } = useReactRouter();

  const handleClick = (e, link) => {
    e.preventDefault();
    history.push(link)
    return
  }
  return (
    <>
      <Button color="primary" onClick={(e) => handleClick(e, "note/create")}>新規作成</Button>
      <List>
        {notes.map((note) => (
          <ListItem key={`${note._id}`}>
            <Button
              color="primary"
              onClick={(e) => handleClick(e, `note/${note._id}`)}
            >{note.title}</Button>
          </ListItem>
        ))}
      </List>
    </>
  );
}
