import React from 'react';
import PropTypes from 'prop-types'
import CodeMirror from '../atoms/NoteCodeMirror.jsx'
import Button from '@material-ui/core/Button';
import api from '../api/backend/index';

export default function NoteEditor(props) {
  const {value, title, onChange, ...others} = props
  async function handleClick(e, value, title) {
    e.preventDefault()
    const postValue = {
      title: title,
      author: "TODO: ",
      body: value,
    }
    return await api.insertNote(postValue)
      .then((success) => {
        return success
      })
      .catch((error) => {
        console.error(error);
        return error
      });
  }

  return (
    <form method="post">
      <CodeMirror
        mode="markdown"
        theme="monokai"
        value={value}
        onChange={onChange}
       />
      <Button
        color="secondary"
        onClick={(e) => handleClick(e, value, title)}
       >送信
      </Button>
    </form>
  )
}

NoteEditor.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string
}

NoteEditor.defaultProps = {
    value: ''
}

