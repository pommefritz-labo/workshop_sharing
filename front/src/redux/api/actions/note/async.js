import {
  getNotes,
  getNotesSuccess,
  getNotesFaild
} from './sync'

export function makePayment(loanId, amount, paymentMethodId) {
  return async dispatch => {
    dispatch(getNotes())
    const res = await api.makePayment(loanId, amount, paymentMethodId)
    if (res.status !== 200) {
      dispatch(getNotesFaild(res.statusText))
    } else {
      const ret = await res.json()
      dispatch(getNotesSuccess(ret.data))
    }
  }
}
