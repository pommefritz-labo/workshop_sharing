export const getNotes = () => ({
  type: defs.GET_NOTES,
})

export const getNotesFaild = (error) => ({
  type: defs.GET_NOTES_FAILED,
  error,
})

export const getNotesSuccess = (notes) => ({
  type: defs.GET_NOTES_SUCCESS,
  notes,
})
