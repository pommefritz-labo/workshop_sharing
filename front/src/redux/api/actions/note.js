export const GET_NOTES = 'GET_MY_PROFILE'

export const getNotes = () => {
  return {
    type: GET_NOTES,
    items: {},
  }
}
