export const GET_MY_PROFILE = 'GET_MY_PROFILE'
export const RES_MY_PROFILE = 'RES_MY_PROFILE'

export const getMyProfile = () => {
  return {
    type: GET_MY_PROFILE,
    items: [],
  }
}

export const resMyProfile = (res) => {
  return {
    type: RES_MY_PROFILE,
    items: res,
  }
}

