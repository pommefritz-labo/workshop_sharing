import { GET_NOTES } from '../actions'

const initialState = {}

export default (state = initialState, action) => {
  switch(action.type) {
    case GET_NOTES:
      return { value: state.value + 1 }
    case DECREMENT:
      return { value: state.value - 1 }
    default:
      return state
      }
    }
  }
}
