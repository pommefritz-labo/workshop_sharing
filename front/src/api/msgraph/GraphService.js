var graph = require('@microsoft/microsoft-graph-client');

function getAuthenticatedClient(accessToken) {
  // Initialize Graph client
  const client = graph.Client.init({
    // Use the provided access token to authenticate
    // requests
    authProvider: (done) => {
      done(null, accessToken.accessToken);
    }
  });

  return client;
}

export async function getUserDetails(accessToken) {
  const client = getAuthenticatedClient(accessToken);

  const user = await client.api('/me').get();
  return user;
}

export async function getEvents(accessToken) {
  const client = getAuthenticatedClient(accessToken);

  const events = await client
    .api('/me/events')
    .select('subject,organizer,start,end,attendees')
    .orderby('createdDateTime DESC')
    .get();

  return events;
}

export async function updateEvents(accessToken, event) {
  const client = getAuthenticatedClient(accessToken);
  console.log(event)

  let res = await client
    .api(`/me/events/${event.id}`)
    .update(event);
  return res
}

export async function getUsers(accessToken) {
  const client = getAuthenticatedClient(accessToken);

  const users = await client
    .api('/users')
    .get();

  return users;
}

export async function getGroups(accessToken) {
  const client = getAuthenticatedClient(accessToken);

  const groups = await client
    .api('/groups')
    .get();

  return groups;
}

export async function getOneNote(accessToken, id) {
  const client = getAuthenticatedClient(accessToken);
  console.log("path")

  const groups = await client
    .api(`/users/${id}/onenote/pages`)
    .get();

  return groups;
}

