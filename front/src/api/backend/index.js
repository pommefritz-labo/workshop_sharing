import axios from 'axios'

const api = axios.create({
      baseURL: 'http://localhost:30000/api',
})

export const insertNote = payload => api.post(`/note`, payload)
export const getAllNotes = () => api.get(`/notes`)
// export const updateMovieById = (id, payload) => api.put(`/note/${id}`, payload)
// export const deleteMovieById = id => api.delete(`/note/${id}`)
export const getNoteById = (id) => api.get(`/note/${id}`)

const apis = {
  insertNote,
  getAllNotes,
  getNoteById,
}

export default apis
