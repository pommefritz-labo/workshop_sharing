const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack')

const distDir = path.join(__dirname, '/dist' );
const publicDir = path.join(__dirname, '/public');

module.exports = {
  mode: 'development',
  entry: { 
    app: ['@babel/polyfill', './src/index.js'],
  },
  output: {
    path: publicDir,
    filename: 'bundle.js',
    publicPath: '/',
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: './public/index.html',
      filename: 'page.html',
      chunks: ['app']
    }),
    new webpack.HotModuleReplacementPlugin()
  ],
  devtool: 'inline-soruce-map',
  module: {
  rules: [{
    test: /\.js(x?)$/,
    exclude: /node_modules/,
    use: {
    loader: 'babel-loader',
    options: {
      presets: ['@babel/preset-env']
      }
    }
  },{
    test: /\.css$/i,
    use: ['style-loader', 'css-loader'],
  }]
  },
  devServer: {
    contentBase: publicDir,
    host: '0.0.0.0',
    historyApiFallback: true,
    compress: true,
    port: 3000,
    hot: true,
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 5000,
    ignored: ['node_modules'],
  }
}
