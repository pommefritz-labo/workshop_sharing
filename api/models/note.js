const mongoose = require('mongoose')
const Schema = mongoose.Schema

const note = new Schema(
      {
          title: { type: String, required: true },
          body: { type: String, required: true },
          author: { type: String, required: true },
          tags: { type: [String], required: false },
      },
  { timestamps: true },
)

module.exports = mongoose.model('notes', note)

