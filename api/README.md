### APIリファレンス

#### エンドポイント
- /api/notes
* Method: GET
Note一覧を取得する

* RETURN VALUE 例:
```
{
    "success": true,
    "data": [
        {
            "tags": [
                "awesome",
                "infra",
                "vs code"
            ],
            "_id": "5ea666407e89680735d5f685",
            "title": "title 4444888888",
            "body": "<p>asfsafsas</p> <p>test test </p> sfefwefwefwefewf",
            "author": "HY",
            "createdAt": "2020-04-27T04:57:36.329Z",
            "updatedAt": "2020-04-27T04:57:36.329Z",
            "__v": 0
        },
        {
            "tags": [],
            "_id": "5ea6666e7e89680735d5f686",
            "title": "my title",
            "author": "TODO: ",
            "body": "### Knowledge\n* aaa\n* bbb\n\n1. ccc\nzzzz\n2. ddd\nyyyy",
            "createdAt": "2020-04-27T04:58:22.874Z",
            "updatedAt": "2020-04-27T04:58:22.874Z",
            "__v": 0
        }
    ]
}
```

- /api/note/<_id>
* Method: GET
<_id>のNoteを取得する

- /api/note/
* Method: POST
NoteをDBに書き込む

* JSON 例:
```
{
    "title": "title 4444888888",
    "body": "<p>asfsafsas</p> <p>test test </p> sfefwefwefwefewf",
    "author": "HY",
    "tags": ["awesome", "infra", "vs code"]
}
```

* RETURN VALUE 例:
```
{
    "success": true,
    "id": "5ea666407e89680735d5f685",
    "message": "Note created!"
}
```

- /api/note/<_id>
* Method: PUT
Noteを更新する
* JSON 例:
```
{
    "title": "title 3",
    "body": "<p>asfsafsas</p> <p>test test </p> afffdfdeeee ",
    "author": "HY",
    "tags": ["awesome", "infra", "vs code"]
}
```

* RETURN VALUE 例:
```
{
    "success": true,
    "id": "5ea666407e89680735d5f685",
    "message": "Note updated!"
}
```

- /api/note/<_id>
* Method: DELETE
Noteを削除する
* RETURN VALUE 例:
```
{
    "success": true,
    "data": {
        "tags": [
            "awesome",
            "infra",
            "vs code"
        ],
        "_id": "5ea668217e89680735d5f687",
        "title": "title 4444888888",
        "body": "<p>asfsafsas</p> <p>test test </p> sfefwefwefwefewf",
        "author": "HY",
        "createdAt": "2020-04-27T05:05:37.978Z",
        "updatedAt": "2020-04-27T05:05:37.978Z",
        "__v": 0
    }
}
```
